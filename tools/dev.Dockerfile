FROM pytorch-dnnevo:latest

RUN pip3 install jupyter

CMD ["jupyter", "notebook", "--port", "8080", "--ip", "0.0.0.0", "--allow-root"]

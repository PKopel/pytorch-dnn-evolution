from torchvision import datasets, transforms
from sys import argv
import pathlib


def mnist(target_dir, is_train_set):
    datasets.MNIST(
        target_dir + '/mnist',
        train=is_train_set,
        download=True,
        transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])
    )


def cifar10(target_dir, is_train_set):
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])
    datasets.CIFAR10(
        root=target_dir + '/cifar10',
        train=is_train_set,
        download=True,
        transform=transform
    )


if __name__ == '__main__':
    if len(argv) > 1:
        target_dir = argv[1]
        pathlib.Path(target_dir).mkdir(parents=True, exist_ok=True)
    else:
        target_dir = '/datasets'

    mnist(target_dir, True)
    mnist(target_dir, False)
    cifar10(target_dir, True)
    cifar10(target_dir, False)

from functools import reduce
from torchvision import datasets, transforms
import os

class DatasetDescriptor(object):

    def __init__(
        self,
        dataset_name,
        input_shape,
        output_shape,
        samples_count,
        dataset_extractor
    ):
        self.__dataset_name = dataset_name
        self.__input_shape = input_shape
        self.__output_shape = output_shape
        self.__samples_count = samples_count
        self.__input_size = reduce(lambda l, r: l*r, input_shape)
        self.__output_size = reduce(lambda l, r: l*r, output_shape)
        self.__dataset_extractor = dataset_extractor

    @property
    def samples_count(self):
        return self.__samples_count

    @property
    def output_shape(self):
        return self.__output_shape

    @property
    def input_shape(self):
        return self.__input_shape

    @property
    def dataset_name(self):
        return self.__dataset_name

    @property
    def input_size(self):
        return self.__input_size

    @property
    def output_size(self):
        return self.__output_size

    @property
    def torch_train_dataset(self):
        return self.__dataset_extractor(is_train_set=True)

    @property
    def torch_test_dataset(self):
        return self.__dataset_extractor(is_train_set=False)


def datasets_directory_prefix():
    datasets_path = os.environ.get('PDE_DATASETS_PATH', 'datasets/')
    return datasets_path


def _get_mnist_dataset(is_train_set):
    prefix = datasets_directory_prefix()
    return datasets.MNIST(
        prefix + 'mnist',
        train=is_train_set,
        download=True,
        transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])
    )


def _get_cifar10_dataset(is_train_set):
    prefix = datasets_directory_prefix()
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])
    return datasets.CIFAR10(
        root=prefix + 'cifar10',
        train=is_train_set,
        download=True,
        transform=transform
    )


dataset_descriptors = {
    'mnist': DatasetDescriptor(
        'mnist',
        (1, 28, 28),
        (10,),
        60000,
        _get_mnist_dataset
    ),
    'cifar10': DatasetDescriptor(
        'cifar10',
        (3, 32, 32),
        (10,),
        50000,
        _get_cifar10_dataset
    ),
}


def retrieve_dataset(dataset_name):
    ds_name = dataset_name.lower()
    if ds_name in dataset_descriptors:
        return dataset_descriptors[ds_name]

    raise Exception("Unknown dataset: " + dataset_name)

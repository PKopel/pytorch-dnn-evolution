from abc import ABC, abstractmethod
from contextlib import contextmanager
import logging
from typing import TypeVar, Generic, Callable, List, NamedTuple, Tuple, Any

T = TypeVar('T')
Listener = Callable[[T], Any]


class Notificator(Generic[T]):
    Ref = Any

    def __init__(self):
        self._listeners = []

    def add_listener(self, listener: Listener[T]) -> Ref:
        self._listeners.append(listener)
        return listener

    def remove_listener(self, listener: Ref):
        self._listeners.remove(listener)

    def listen_to(self, other: 'Notificator[T]', decorator: Callable[[T], T] = lambda x: x) -> Ref:
        return other.add_listener(lambda v: self._notify(decorator(v)))

    def _notify(self, value: T):
        if not self._listeners:
            logging.getLogger('notificator').warning('Nobody is listening to progress tracking!')

        for listener in self._listeners:
            listener(value)


class IterationStatus(NamedTuple):
    current_no: int
    current_progress: int


class IterationTracker(ABC, Notificator[IterationStatus]):
    @property
    @abstractmethod
    def current_no(self) -> int: ...

    @property
    @abstractmethod
    def current_progress(self) -> int: ...

    @property
    @abstractmethod
    def current_target(self) -> int: ...

    def status(self) -> IterationStatus:
        return IterationStatus(
            current_no=self.current_no,
            current_progress=self.current_progress,
        )


class StatefulIterationTracker(IterationTracker):
    def __init__(self, log, initial_no: int = 0, initial_target: int = 0):
        super().__init__()

        self._current_no = initial_no
        self._current_target = initial_target
        self._current_ticks = 0
        self._log = log

    @property
    def current_no(self) -> int:
        return self._current_no

    @property
    def current_progress(self) -> int:
        # if nothing to evaluate - we are done
        if self._current_target == 0:
            return 100

        return int(100 * (self._current_ticks / self._current_target))

    @property
    def current_target(self) -> int:
        return self._current_target

    @contextmanager
    def enter(self) -> 'IterationSection':
        self._log.debug(f"Iteration {self.current_no} start")

        yield self.IterationSection(self)

        self._current_ticks = 0
        self._current_no += 1

        self._log.debug(f"Iteration {self.current_no} end")

    class IterationSection:
        def __init__(self, tracker: 'StatefulIterationTracker'):
            self._tracker = tracker

        def set_target(self, target: int):
            self._tracker._current_target = target
            self._tracker._log.debug(f"To evaluate: {self._tracker._current_target}")

        def tick(self) -> None:
            self._tracker._current_ticks += 1
            self._tracker._notify(self._tracker.status())


class CompositeIterationTracker(IterationTracker):
    steps: List[IterationTracker]

    def __init__(self, log, steps: List[IterationTracker]):
        super().__init__()

        self._log = log

        self.steps = steps
        for step in self.steps:
            self.listen_to(step, lambda _: self.status())

    @property
    def current_no(self) -> int:
        return max(it.current_no for it in self.steps)

    @property
    def current_progress(self) -> int:
        _, step = self.current_step
        return step.current_progress

    @property
    def current_target(self) -> int:
        _, step = self.current_step
        return step.current_target

    @property
    def current_step(self) -> Tuple[int, IterationTracker]:
        for i, step in enumerate(self.steps):
            if step.current_progress < 100:
                return i, step

        return len(self.steps) - 1, self.steps[-1]

    @contextmanager
    def with_special_section(self) -> StatefulIterationTracker.IterationSection:
        step = StatefulIterationTracker(self._log, initial_no=self.current_no)
        self.listen_to(step, lambda _: self.status())

        prev_steps = self.steps
        self.steps = [step]
        try:
            with step.enter() as section:
                yield section
        finally:
            self.steps = prev_steps

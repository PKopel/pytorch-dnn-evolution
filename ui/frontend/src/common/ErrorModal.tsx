import * as React from 'react';
import { Modal, Button } from 'react-bootstrap';
import './Common.css';

interface ErrorModalProps {
    show: boolean,
    title: string,
    message: string,
    onClose: () => void
}

export const ErrorModal: React.StatelessComponent<ErrorModalProps> = (props) => {
    if (!props.show) {
        return null;
    }
    return (
        <div className="ErrorModal-container">
            <Modal
                show={props.show}
                animation={true}
                onHide={props.onClose}
            >
                <Modal.Header>
                    {props.title}
                </Modal.Header>
                <Modal.Body>
                    {props.message}
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        bsStyle="warning"
                        onClick={props.onClose}
                    >
                        OK
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

import * as React from 'react';
import {SocketContextProps} from '../common';

interface Props extends SocketContextProps {
    prop1?: string,
}

interface State {
    prop1?: string
}

export class ExperimentsViewContainer extends React.Component<Props, State> {

}

import * as React from 'react';
import {DatabaseView} from './DatabaseView';
import {SocketContextProps} from '../common';
import {withSocketContext} from "../common/SocketContext";

class DatabaseViewContainer extends React.Component<SocketContextProps, any> {
    public constructor(props) {
        super(props);
        this.state = {
            response: ''
        };
        this.props.socket.on('dbimport_response', this.handleDbImport);
    }

    public render() {
        return (
            <DatabaseView importDataBase={this.importDataBase} response={this.state.response}
            />
        );
    }

    public componentWillUnmount() {
        this.props.socket.off('dbimport_response', this.handleDbImport);
    }

    public importDataBase = (value: string) => {
        this.props.socket.emit("dbimport", {dump: value});
    }

    private handleDbImport = (message) => {
        const msg = JSON.parse(message);
        this.setState({response: msg.return_value}, () => this.forceUpdate());
    }
}


export default withSocketContext(DatabaseViewContainer);
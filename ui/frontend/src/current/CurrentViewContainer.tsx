import * as React from 'react';
import ProgressBarGroupContainer from "./ProgressBarGroupContainer";
import ChartGroupContainer from "./main/ChartGroupContainer";
import GraphGroupContainer from "./graph/GraphGroupContainer";


export class CurrentViewContainer extends React.Component<any, any> {

    public constructor(props: any) {
        super(props);
    }

    public render() {
        return (
            <div>
                <ProgressBarGroupContainer/>
                <ChartGroupContainer/>
                <GraphGroupContainer/>
            </div>
        );
    }
}

import * as React from "react";
import { Line } from "react-chartjs-2";

import "./Chart.css";
// import { triggerAsyncId } from "async_hooks";

interface Props {
  labels: any;
  values: any;
  title: string;
  append: boolean;
}

interface State {
  data?: any;
  mapping: any;
}

export class MainChart extends React.Component<Props, State> {
  private static getOptions(): any {
    return {
      options: {
        animation: {
          duration: 250 * 1.5,
          easing: "linear"
        },
        legend: false,
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          xAxes: [
            {
              display: true,
              labelString: "Iteration"
            }
          ],
          yAxes: [
            {
              display: true,
              ticks: {
                max: 100,
                min: 0
              }
            }
          ]
        }
      }
    };
  }

  private readonly title: string;

  private readonly colors: any = [
    "#ff6384", // red
    "#36a2eb", // blue
    "#cc65fe", // purple
    "#ffce56", // yellow
    "#63ffc6" // green
  ];

  public constructor(props: Props) {
    super(props);
    this.title = props.title;
    this.state = {
      data: { datasets: [], labels: [] },
      mapping: {}
    };
  }

  public render() {
    return (
      <div className="chart-container">
        <h1>{this.title}</h1>
        <Line
          data={this.state.data}
          options={MainChart.getOptions()}
          key={Math.random()}
        />
      </div>
    );
  }

  public componentWillReceiveProps(props: Props) {
    this.setData(props);
  }

  private hasData() {
    return this.state.data.datasets.length > 0;
  }

  private updateIsNew(props: Props) {
    const pointsNo = this.state.data.datasets[0].data.length;
    const lastKnownIteration = pointsNo - 1;

    const genIndex = props.labels.indexOf("gen");
    if (genIndex === -1) {
      console.error(
        "'gen' sequence not found in sent data! Please check the source code "
      );
      return false;
    }

    const firstIterationOfUpdate = props.values[genIndex][0];

    return firstIterationOfUpdate > lastKnownIteration;
  }

  private setData(props: Props): void {
    if (!this.hasData()) {
      let colorIdx = 0;
      props.values.forEach((value, index) => {
        const idxLabel = props.labels[index];
        if (idxLabel !== "gen") {
          this.state.data.datasets.push({
            borderColor: this.colors[colorIdx++],
            borderWidth: 2,
            data: value,
            label: idxLabel,
            lineTension: 0.25,
            fill: false
          });
          this.state.mapping[idxLabel] = this.state.data.datasets.length - 1;
        }
      });
    } else {
      if (this.updateIsNew(props)) {
        props.values.forEach((value, index) => {
          const label = props.labels[index];
          const idxForLabel = this.state.mapping[label];
          if (label !== "gen") {
            const dataset = this.state.data.datasets[idxForLabel];
            if (props.append) {
              value.forEach(val => dataset.data.push(val));
            } else {
              const currentData = dataset.data;
              dataset.data = value.concat(currentData);
            }
          }
        });
      }
    }

    const pointsNo = this.state.data.datasets[0].data.length;
    const labels = [];

    for (let i = 0; i < pointsNo; i++) {
      labels.push(i + 1);
    }

    this.state.data.labels = labels;
  }
}

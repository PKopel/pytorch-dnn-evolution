import * as React from 'react';

interface GraphProps {
    containerId: string
}

export const Graph: React.StatelessComponent<GraphProps> = (props) => {
    return (<div id={props.containerId}/>);
};
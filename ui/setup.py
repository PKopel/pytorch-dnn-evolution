from setuptools import setup

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
    name='torch-dnnevo-ui',
    version='0.1',
    description='Web UI for the torch-dnnevo',
    author='Pawel Koperek',
    author_email='pkoperek@gmail.com',
    url='https://gitlab.com/pkoperek/pytorch-dnn-evolution',
    packages=['torchxui'],
    long_description=long_description,
    install_requires=[
        'torch-dnnevo',
        'numpy',
        'matplotlib',
        'flask-admin',
        'flask-sqlalchemy',
        'networkx',
        'celery',
        'pydot',
        'flask-socketio==4.3.2',
        'psycopg2-binary',
        'eventlet',
        'py-cpuinfo',
    ],
    entry_points={
        'console_scripts': ['evomgr=torchxui.main:main']
    }
)
